package ru.t1.dkozyaikin.tm.command.task;

import ru.t1.dkozyaikin.tm.exception.AbstractException;

public final class TaskClearCommand extends AbstractTaskCommand {

    public static final String NAME = "task-clear";

    public static final String DESCRIPTION = "Clear all tasks";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CLEAR TASK LIST]");
        final String userId = getAuthService().getUserId();
        getTaskService().clear(userId);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
