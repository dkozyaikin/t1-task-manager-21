package ru.t1.dkozyaikin.tm.command.user;

import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.user.AccessDeniedException;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    public static final String NAME = "update-profile";

    public static final String DESCRIPTION = "Update user profile";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UPDATE USER PROFILE]");
        final String userId = getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        System.out.println("ENTER FIRST NAME:");
        final String fistName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, fistName,lastName, middleName);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
