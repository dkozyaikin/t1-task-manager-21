package ru.t1.dkozyaikin.tm.command.system;

import ru.t1.dkozyaikin.tm.exception.AbstractException;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    public static final String NAME = "version";

    public static final String ARGUMENT = "-v";

    public static final String DESCRIPTION = "Show application version";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[VERSION]");
        System.out.println("1.21.0");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
