package ru.t1.dkozyaikin.tm.command.task;

import ru.t1.dkozyaikin.tm.enumerated.Sort;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.Task;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    public static final String NAME ="task-list";

    public static final String DESCRIPTION = "Show all tasks.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getAuthService().getUserId();
        final List<Task> tasks = getTaskService().findAll(userId, sort);
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
