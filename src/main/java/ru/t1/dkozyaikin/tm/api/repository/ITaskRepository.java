package ru.t1.dkozyaikin.tm.api.repository;

import ru.t1.dkozyaikin.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    List<Task> findAllTasksByProjectId(String userId, String projectId);

}

