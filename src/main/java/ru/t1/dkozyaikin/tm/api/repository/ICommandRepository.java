package ru.t1.dkozyaikin.tm.api.repository;

import ru.t1.dkozyaikin.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand command);

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String argument);

    Collection<AbstractCommand> getTerminalCommands();

}
