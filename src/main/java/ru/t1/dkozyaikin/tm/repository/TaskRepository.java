package ru.t1.dkozyaikin.tm.repository;

import ru.t1.dkozyaikin.tm.api.repository.ITaskRepository;
import ru.t1.dkozyaikin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(String userId, String name) {
        Task task = new Task(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(String userId, String name, String description) {
        Task task = new Task(name, description);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public List<Task> findAllTasksByProjectId(String userId, final String projectId) {
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

}
